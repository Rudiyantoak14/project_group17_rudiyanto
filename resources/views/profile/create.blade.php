@extends('layouts/master')
@push('script')
    <script src="{{ asset('js/script.js') }}"></script>
@endpush
@section('judul')
Input Tabel Kategori Media Sosial

@endsection

@section('content')

<form action="/profile" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label>Umur: </label>
        <input type="text" name="Umurmu"><br>

            @error('Umurmu')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror

        
        <label>Bio:</label><br>
        <textarea name="Biomu" cols="30"rows="5"></textarea><br>

            @error('Biomu')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror

        

    </div>

   
   
    <button type="submit" class="btn btn-primary">Submit</button>
</form>   

@endsection