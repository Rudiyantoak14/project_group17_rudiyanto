@extends('layouts/master')
@push('script')
    <script src="{{ asset('js/script.js') }}"></script>
@endpush
@section('judul')
List isi dari Tabel Kategori Media Sosial

@endsection

@section('content')
<a href="/profile/create" class="btn btn-success mb-2" >Input Profil Baru</a>

        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Umur</th>
                <th scope="col">Bio</th>
                <th scope="col">Alamat</th>
                <th scope="col">Aksi</th>
            </tr>
            </thead>
            <tbody>
                @forelse ($VarCast as $key => $item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->umur}}</td>
                    <td>{{$item->bio}}</td>
                    <td>{{$item->alamat}}</td>

                    <td><a href="/kategori/{{$item->id}}" class="btn btn-info btn-sm" >Detail</a>
                        <a href="/kategori/{{$item->id}}/edit" class="btn btn-warning btn-sm" >Edit</a>
                        <form action="/kategori/{{$item->id}}" method="POST">
                            @method('delete')
                            @csrf

                            <input type="submit" class="btn btn-danger btn-sm" value="Delete">

                        </form>
                        
                    <td>
                </tr>
                    
                @empty
                <tr>
                    <td> Tidak ada DATA</td>
                </tr>
                    
                @endforelse
            </tbody>
        </table>
        
       

@endsection