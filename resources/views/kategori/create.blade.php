@extends('layouts/master')
@push('script')
    <script src="{{ asset('js/script.js') }}"></script>
@endpush
@section('judul')
Input Tabel Kategori Media Sosial

@endsection

@section('content')

<form action="/kategori" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label>Nama/Jenis Kategori: </label>
        <input type="text" name="Jenismu"><br>

            @error('Jenismu')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror

        
        <label>Deskripsi/keterangan dari kategori ini:</label><br>
        <textarea name="Deskripsimu" cols="30"rows="5"></textarea><br>

            @error('Deskripsimu')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror

        

    </div>

   
   
    <button type="submit" class="btn btn-primary">Submit</button>
</form>   

@endsection