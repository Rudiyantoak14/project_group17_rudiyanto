@extends('layouts/master')
@push('script')
    <script src="{{ asset('js/script.js') }}"></script>
@endpush
@section('judul')
List isi dari Tabel Psoting Media Sosial

@endsection

@section('content')


<a href="/likes/create" class="btn btn-success mb-2" >Input Likes Baru</a>

        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Postingan yang disukai</th>
                <th scope="col">User yang menyukai</th>
                <th scope="col">Jumlah Bintang</th>
                <th scope="col">Aksi</th>
            </tr>
            </thead>
            <tbody>
                @forelse ($VarCast as $key => $item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->user_id }}</td>
                    <td>{{$item->posts_id }}</td>
                    <td>{{$item->likebintang}}</td>

                    <td><a href="/likes/{{$item->id}}" class="btn btn-info btn-sm" >Detail</a>
                        <a href="/likes/{{$item->id}}/edit" class="btn btn-warning btn-sm" >Edit</a>
                        <form action="/likes/{{$item->id}}" method="POST">
                            @method('delete')
                            @csrf

                            <input type="submit" class="btn btn-danger btn-sm" value="Delete">

                        </form>
                        
                    <td>
                </tr>
                    
                @empty
                <tr>
                    <td> Tidak ada DATA</td>
                </tr>
                    
                @endforelse
            </tbody>
        </table> 
       

    @endsection