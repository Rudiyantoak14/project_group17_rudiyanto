@push('style')
    <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
@endpush

<aside class="main-sidebar sidebar-light-primary" style="opacity: .90">
    <!-- Brand Logo -->
    <a href="../../index3.html" class="brand-link">
      <img src="{{ asset('admin/dist/img/logogroup17.png') }}" alt="Team17 Logo" class="brand-image elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">Group17</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ asset('admin/dist/img/logodepan.jpg') }}" class="rounded-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">Group17</a>
        </div>
      </div>

      <!-- SidebarSearch Form -->
      <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="/profil" class="nav-link">
              <i class="nav-icon fas fa-user"></i>
              <p>
                Profil
                <i class="right"></i>
              </p>
            </a>            
          </li>

          <li class="nav-item">
            <a href="/table" class="nav-link">
              <i class="nav-icon fas fa-table"></i>
              <p>
                Tables                
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="/games" class="nav-link">
              <i class="nav-icon fas fa-basketball-ball"></i>
              <p>
                Game                
              </p>
            </a>
          </li>

          
          
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>