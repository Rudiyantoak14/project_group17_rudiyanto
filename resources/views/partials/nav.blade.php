<nav class="main-header navbar navbar-expand navbar-light navbar-light" style="opacity: .90">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="/dashboard" class="nav-link">Dashboard</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Contact</a>
      </li>
    </ul>

    

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Navbar Search -->
      <li class="nav-item">
        <a class="nav-link" data-widget="navbar-search" href="#" role="button">
          <i class="fas fa-search"></i>
        </a>
        <div class="navbar-search-block">
          <form class="form-inline">
            <div class="input-group input-group-sm">
              <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
              <div class="input-group-append">
                <button class="btn btn-navbar" type="submit">
                  <i class="fas fa-search"></i>
                </button>
                <button class="btn btn-navbar" type="button" data-widget="navbar-search">
                  <i class="fas fa-times"></i>
                </button>
              </div>
            </div>
          </form>
        </div>
      </li>
    </ul>

    <ul class="navbar-nav float-right">
      @auth
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false">Selamat datang kembali, {{ auth()->user()->name }}</a>
            <div class="dropdown-menu">
              <a class="dropdown-item" href="/dashboard"><i class="fas fa-columns"></i> Dashbord</a>
              <div class="dropdown-divider"></div>
              <form action="/logout" method="POST">
                @csrf
                <button type="submit" class="dropdown-item"><i class="fas fa-sign-out-alt"></i> Logout</button>
              </form>
            </div>
          </li>
      @else
      <li class="nav-item">
        <a href="/login" class="nav-link"><i class="fas fa-sign-in-alt"></i> Login</a>
      </li>
      @endauth
    </ul>
      <!-- Messages Dropdown Menu -->
      
      <!-- Notifications Dropdown Menu -->
      
  </nav>