@extends('layouts/masterProfil')



    {{-- Nav Profil --}}
    @include('partials/navProfil')
    {{-- Akhir Nav Profil --}}

    
@section('content')
    {{-- Jumbotron --}}
    <main role="main" class="container ">
      <div class="jumbotron bg-white">
        <img src="{{ asset('img/Jumbotron.png') }}" class="card-img-top img-fluid  mb-3" style="height: 350px; " alt="...">
        <a class="btn btn-sm btn-primary float-right" href="../../components/navbar/" role="button"><i class="fas fa-camera"></i> Edit Background</a>
      </div>
    </main>
    {{-- Akhir Jumbotron --}}


    {{-- Input Postingan --}}
    <div class="container mb-3 ">
    <div class="card" style="max-width: 100%;">
      <div class="card-body">
        
        <div class="input-group mb-3 border-warning">
          <div class="input-group-prepend border-warning">
            <img src="{{ asset('img/Foto Pratama.jpg') }}" class="rounded-circle mr-3 rounded-pill" style="height: 40px; width: 40px;" alt="">
          </div>
          <button type="button" class="btn btn-light text-left  rounded-pill" style="width: 800px;" data-toggle="modal" data-target="#PostProfil" aria-pressed="false">
            Apa yang Anda pikirkan, {{ auth()->user()->name }}
          </button>
          {{-- <a href="#" class="btn btn-light  rounded-pill "  data-toggle="modal" data-target="#PostModal">Apa yang Anda pikirkan, Nama User</a> --}}
          
        </div>
        <div class="container">
          <hr class="border-dark">
        </div>

        <a href="#" class="btn btn-light float-right" type="file"> <i class="fas fa-camera"></i> Foto</a>
      </div>
    </div>
    </div>
    {{-- Akhir Postingan --}}

    <!-- Modal -->
    <div class="modal fade" id="PostProfil" tabindex="-1" aria-labelledby="PostModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="PostModalLabel">Postingan Anda</h5>
            <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary " style="width: 100%">Kirim</button>
          </div>
        </div>
      </div>
    </div>
    {{-- Akhir Modal --}}

    {{-- Tampilan Postingan dan Komentar --}}
    <div class="container justify-content-center">
    <div class="card border-warning mb-3 " style="max-width: 100%;">
      <div class="card-header bg-transparent border-warning">
        
        <ul class="nav justify-content-start">
          <li class="nav-item">
            <img src="{{ asset('img/Foto Pratama.jpg') }}" class="rounded-circle" style="height: 40px; width: 40px;" alt="">
          </li>
          <li class="nav-item">
            <a class="nav-link text-dark" href="#">Pratama Adi Ciptawan</a>
          </li>          
        </ul>

      </div>
      <div class="card-body text-black">
        <h5 class="card-title">Komentar disini ya ....</h5>
      </div>
      <div class="card-footer bg-transparent border-warning">Footer</div>
    </div>
    </div>

    {{-- Akhir Postingan --}}

@endsection

    
