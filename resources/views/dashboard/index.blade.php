@extends('layouts/master')
@push('script')
    <script src="{{ asset('js/script.js') }}"></script>
@endpush
@section('judul')
Media Online

@endsection

@section('content')



{{-- Input Postingan --}}
    <div class="container mb-3 ">
    <div class="card" style="max-width: 100%;">
      <div class="card-body">
        
        <div class="input-group mb-3 border-warning">
          <div class="input-group-prepend border-warning">
            <img src="{{ asset('img/Foto Pratama.jpg') }}" class="rounded-circle mr-3 rounded-pill" style="height: 40px; width: 40px;" alt="">
          </div>
          <button type="button" class="btn btn-light text-left  rounded-pill" style="width: 700px;" data-toggle="modal" data-target="#PostModal" aria-pressed="false">
            Apa yang Anda pikirkan, 
            {{-- {{ auth()->user()->name }} --}}
          </button>
          {{-- <a href="#" class="btn btn-light  rounded-pill "  data-toggle="modal" data-target="#PostModal">Apa yang Anda pikirkan, Nama User</a> --}}
          
        </div>
        <div class="container">
          <hr class="border-dark">
        </div>

        <a href="#" class="btn btn-light float-right" type="file"> <i class="fas fa-camera"></i> Foto</a>
      </div>
    </div>
    </div>
    {{-- Akhir Postingan --}}

    {{-- Modal --}}


<!-- Modal -->
<div class="modal fade" id="PostModal" tabindex="-1" aria-labelledby="PostModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="PostModalLabel">Postingan Anda</h5>
        <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary " style="width: 100%">Kirim</button>
      </div>
    </div>
  </div>
</div>
{{-- Akhir Modal --}}

    {{-- Tampilan Semua Postingan Home --}}

    <div class="container justify-content-center">
        <div class="card  mb-3 " >
          <div class="card-header bg-transparent ">
            
            <ul class="nav justify-content-start">
              <li class="nav-item">
                <img src="{{ asset('img/Foto Pratama.jpg') }}" class="rounded-circle" style="height: 40px; width: 40px;" alt="">
              </li>
              <li class="nav-item">
                <a class="nav-link text-dark" href="#">
                  {{-- {{ auth()->user()->name }} --}}
                </a>
              </li>          
            </ul>

          </div>
          <div class="card-body text-black">
            {{-- Komentar Disini --}}

            <p class="card-text">Bentar pak belum ngeh yayaya ini</p>

            
  
            

            {{-- Like --}}

            <i class="fas fa-thumbs-up font-weight-normal"> 132</i>
            <h5 class="float-right font-weight-normal">12 Komentar</h5>

            {{-- Akhir Like --}}

            <hr class="border-dark" >

            <div class="btn-toolbar justify-content-around" role="toolbar" aria-label="Toolbar with button groups">
              <div class="btn-group" role="group" aria-label="First group">
                <button type="button" class="btn btn-warning font-weight-normal"><i class="far fa-thumbs-up"></i> Like</i></button>
                
              </div>
              <div class="btn-group" role="group" aria-label="Second group">
                <button type="button" class="btn btn-warning" id="komentarPost"><i class="far fa-comment-alt"> Komentar</i></button>
                
              </div>
              
            </div>

            <hr class="border-dark" >
          </div>

          {{-- Komentar user lain --}}
            <div class="container" id="isiKomentar">
            {{-- disini akan muncul komentarnya --}}
            </div>

        </div>
    </div>
  {{-- Akhir Semua Postingan Home --}}

@endsection