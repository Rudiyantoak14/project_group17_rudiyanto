<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\posts;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $VarCast=posts::all();
        return view('posts.index',compact('VarCast')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $VarCast = DB::table('posts')->get();
        $kategori = DB::table('kategori')->get();
        return view('posts.create', compact('kategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'Tanggalmu' => 'required',
            'Isipostsmu' => 'required',   
             'kategori_idmu' => 'required',    
              'Fotomu' => 'required|image|mimes:jpeg,png,jpg|max:2048',          
            ]);

            $namaThumbnail = time().'.'.$request->Fotomu->extension();       //ganti nama filenya menjadi time 
             $request->Fotomu->move(public_path('img'),$namaThumbnail); // masukkan gambar di folder public/kumpulan gambar 


            $posts = new Posts;

            $posts ->tanggal = $request->Tanggalmu;
            $posts ->isiposts = $request->Isipostsmu;
             $posts ->kategori_id = $request->kategori_idmu;
              $posts->fotothumbnail = $namaThumbnail;

             $posts ->save();

            return redirect('/posts');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
