<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\likes;

class LikesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $VarCast=likes::all();
        return view('likes.index',compact('VarCast')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
                
                 $postslikes = DB::table('posts')->get();                
                 return view('likes.create', compact('postslikes'));

                // $userlikes = DB::table('users')->get();
                // return view('likes.create', compact('userlikes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'Userlikemu' => 'required',
            'Postinganlikemu' => 'required',               
            'likebintangmu' => 'required',           
            ]);

            $likes = new Likes;

            $likes ->user_id = $request->Userlikemu;
            $likes ->posts_id = $request->Postinganlikemu;
            $likes ->likebintang = $request->likebintangmu;
             

             $likes ->save();

            return redirect('/likes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
