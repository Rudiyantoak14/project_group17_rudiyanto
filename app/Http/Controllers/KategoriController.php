<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\kategori;

class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $VarCast=kategori::all();
        return view('kategori.index',compact('VarCast')); 

        //Boleh pili dibawah ini sama saja hasilnya
        // $VarCast = DB::table('kategori')->get();
        // return view('kategori.index', compact('VarCast')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $VarCast = DB::table('kategori')->get();
        return view('kategori.create', compact('VarCast'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'Jenismu' => 'required',
            'Deskripsimu' => 'required',              
            ]);
            $kategori = new Kategori;

            $kategori->jenis = $request->Jenismu;
            $kategori->deskripsi = $request->Deskripsimu;
            $kategori->save();

            return redirect('/kategori');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //  Bagian ini adalah untuk menampilkan detail

        $VarCast=kategori::findOrFail($id);

       // $VarId = DB::table('case')->where('id',$id)->first();
        return view('kategori.show',compact('VarCast'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $VarCast = DB::table('kategori')->get();   //->where('id',$id)->first();
        // $VarCast=kategori::findOrFail($id);

        $VarId = DB::table('kategori')->where('id',$id)->first();
        return view('kategori.edit',compact('VarId'));
    

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
