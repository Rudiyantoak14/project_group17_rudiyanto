<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kategori extends Model
{
    protected $table = 'kategori'; //nama Tabelnya
    protected $fillable = ["jenis","deskripsi"];
}
